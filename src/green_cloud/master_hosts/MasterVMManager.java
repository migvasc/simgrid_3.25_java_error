package green_cloud.master_hosts;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.simgrid.msg.Host;
import org.simgrid.msg.HostFailureException;
import org.simgrid.msg.HostNotFoundException;
import org.simgrid.msg.Msg;
import org.simgrid.msg.MsgException;
import org.simgrid.msg.Process;

import green_cloud.energy.ElectricGrid;

import green_cloud.vm.VmDescription;
import green_cloud.workers_hosts.WorkerVMManager;

/**
 * Master application managing the distributed cloud with on-site green (e.g. solar) energy productions.
 * The cloud is composed of heterogeneous hosts. 
 * The master is in charge of dispatching an incoming workload to the different data-centers.
 * Rather than directly interacting with the hosts, the master deploy and interact with worker applications which encapsulate the hosts desired behavior. 
 * The workload is composed of VMs which require a given number of cores and has to be executed for a given period of time.
 * The master, and therefore the workload, have a time-stepped behavior.
 * @author bcamus
 *
 */
public abstract class MasterVMManager extends Process {
	
	/**
	 * Character used to separate values in the input and output traces.
	 */
	public final static String FILE_SEPARATOR = ";";
	
	/**
	 * Map of all workers gathered by data-centers.
	 */
	protected static HashMap<String,ArrayList<WorkerVMManager>> workerList;
	
	/**
	 * Electrical grid of the cloud. 
	 */
	protected static ElectricGrid grid;;
	
	/**
	 * Duration a host takes to power ON.
	 */
	protected double hostPoweringOnDelay;
	
	/**
	 * Duration a host takes to power OFF.
	 */
	protected double hostPoweringOffDelay;
	
	/**
	 * The input stream used to read the incoming workload.
	 */
	private BufferedReader inputWorkload;
	
	/**
	 * Factor used to scale the incoming workload to the cloud. It simply duplicate each incoming VM of the workload.
	 */
	private int workloadScaleFactor;

	/**
	 * Duration of a timeslot. At each timeslot, the master receive new VMs and dispatch them in the cloud.
	 */
	protected int timeslot;
	
	/**
	 * RAM size of the VM per core required.
	 */
	public int vmRamSizePerCore;
	

	
	
	protected boolean shutDown;
	
	
	/**
	 * Create a master application and install it in a given host of the cloud.
	 * 
	 * @param host The host where the master must be executed.
	 * @param dcName The name of the data-centers which must be considered by the master.
	 * @param aHostPoweringOnDelay Duration a host takes to power ON.
	 * @param aHostPoweringOffDelay Duration a host takes to power OFF.
	 * @param inputWorkloadPath CSV file containing the incoming workload (line format : VM-arrival-time;VM-name;number-of-cores;duration).
	 * @param aWorkloadScaleFactor Factor used to scale the incoming workload to the cloud.
	 * @param inputGreenProductions CSV files containing the green power production of one photo-voltaic panel of each data-centers (line format : time;power-production).
	 * @param numberOfPVPerDC numbers of photo-voltaic panels (used to multiply the green production).
	 * @param aTimeslot duration of a time-slot.
	 * @param powerLossInMigration The ratio of power lost when migrating power from one DC to another one.
	 * @param aVmRamSizePerCore RAM size of the VM per core required.
	 * @throws HostNotFoundException 
	 */
	public MasterVMManager (Host host, String[] dcName, double aHostPoweringOnDelay, double aHostPoweringOffDelay, String inputWorkloadPath, int aWorkloadScaleFactor, String[] inputGreenProductions, int[] numberOfPVPerDC, int aTimeslot, double powerLossInMigration, int aVmRamSizePerCore,  double aBuying_cost, double aSelling_gain, double anAutoconso_cost, double anAutoconso_gain, double aFullUtilisationCost, double aLightUtilisationCost, double aFullCostPeriod, double aLightCostPeriod, double autoconsoPeriod, double sellBuyPeriod, boolean shut_down) throws HostNotFoundException  {
	    super(host, "master");
	    hostPoweringOnDelay = aHostPoweringOnDelay;
	    hostPoweringOffDelay = aHostPoweringOffDelay;
	    
	    workerList = new HashMap<String,ArrayList<WorkerVMManager>>();
	    for(String dc : dcName){
	    	workerList.put(dc, new ArrayList<WorkerVMManager>());
	    }
	    
	  //  migrationLogger = new MigrationsLogger(dcName);
	    
	    try {
			inputWorkload = new BufferedReader(new FileReader(inputWorkloadPath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println(e);
			System.out.println(e.getMessage());
			System.out.println(new Date());
		}
	    	    
	    timeslot = aTimeslot;
	    
	    grid = new ElectricGrid(dcName, getHost(), inputGreenProductions, numberOfPVPerDC, powerLossInMigration, aBuying_cost, aSelling_gain, anAutoconso_cost, anAutoconso_gain, aFullUtilisationCost, aLightUtilisationCost, aFullCostPeriod, aLightCostPeriod, autoconsoPeriod, sellBuyPeriod);
	    
	    workloadScaleFactor = aWorkloadScaleFactor;
	    
	    vmRamSizePerCore = aVmRamSizePerCore;
	    
	    shutDown = shut_down;

	 }
	
	
	
	
	@Override
	/**
	 * Behavior of the master. It contains the main simulation loop.
	 */
	public void main(String[] arg0) throws MsgException, HostNotFoundException, HostFailureException { 
		
		deployment();

		String incomingVmAction;
				
		try {
			
			incomingVmAction = inputWorkload.readLine();
			String[] vmActionFeatures = incomingVmAction.split(";");
			double actionTime = Double.parseDouble(vmActionFeatures[0]);
			String vmName = vmActionFeatures[1];
			int nbCores = Integer.parseInt(vmActionFeatures[2]);
			double duration = Double.parseDouble(vmActionFeatures[3]);
			double use = 1.0;
			if(vmActionFeatures.length>=5){
				use = Double.parseDouble(vmActionFeatures[4]);
			}
			
			// we parse the whole workload
			while(incomingVmAction != null  && Msg.getClock()<= 30000000){
				
				ArrayList<VmDescription> vms = new ArrayList<VmDescription>();
				
				// we wait until the next time-slot.
				waitFor(timeslot);
				
				deployVMs(vms);

			
			}
			
			Msg.info("Workload finished, killing all the workers still runing");
			System.out.println(new Date());
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
			System.out.println(e);
			System.out.println(e.getMessage());
			System.out.println(new Date());
			
		}
	    
	}



	
	/**
	 * Deploy a set of VMs arriving at the same time.
	 * @param vms the list of VM arriving at the current simulation time.
	 * @throws MsgException
	 */
	protected abstract void deployVMs(ArrayList<VmDescription> vms) throws MsgException;
	


	
	/**
	 * Performs the initial deployment. It install a worker on each host and power OFF each host.
	 * After this method, the cloud is in the initial state (simulation is still at time 0)
	 * @throws HostNotFoundException
	 */
	private void deployment() throws HostNotFoundException {
		
		Msg.info("starting PV power production monitoring applications");
		grid.startPvProcess();
		
		Msg.info("installing workerVmManagers on hosts");
		Host hosts[] = Host.all();
		int totalHosts=0;
	    for(Host host : hosts){
	    	
	    	// we do not install workers in the same host than the master to prevent shut down the master.
	    	if(host != getHost()){
		    	
		    	for(String dcName : workerList.keySet()){
		    	
		    		if(host.getName().contains(dcName)){
		    			// create the worker	    		
				    	WorkerVMManager worker = new WorkerVMManager(host,"workerVmManager_"+host.getName(),hostPoweringOnDelay,hostPoweringOffDelay,dcName,this);				    					    	
				    	worker.start();
				    	worker.setAutoRestart(true);
				    	if(shutDown){
				    		host.off();				    	
		    			}				 
				    	
				    	// add it to the list
		    			workerList.get(dcName).add(worker);
		    			totalHosts++;
		    		}
		    	}
	    	}
	   	
	    	/** 
	    	 * In the XML platform file there is 1K+ hosts, but with 3 hosts the error sent in the e-mail occurs, with all the hosts, other error occurs for this code example.
	    	 * In the original code, that considers all the files, the error sent in the email occurs. 
	    	 */
 
	    	if(totalHosts==3) break;
	    	
	    	
	    }
	}
	
	/**
	 * Start the powering ON process in a given host
	 * @param workerToSetOn the worker which host has to be set ON
	 * @return the new worker (because autostart does not work)
	 * @throws MsgException
	 */
	// TODO: it returns the new worker cause autostart does not work. We can remove this when autostart is fixed
	public WorkerVMManager powerOnHost(WorkerVMManager workerToSetOn) throws MsgException{		
		Msg.info("trying to power ON host "+workerToSetOn.getHost().getName());
		Host hostToSetOn = workerToSetOn.getHost();		
		hostToSetOn.on();		
		return workerToSetOn;
		
	}


	/**
	 * Get the map of all the workers deployed on the cloud gathered by data-centers
	 * @return the map of all the workers deployed on the cloud gathered by data-centers
	 */
	public static HashMap<String,ArrayList<WorkerVMManager>> getWorkerList(){
		return workerList;
	}
	
	
}
