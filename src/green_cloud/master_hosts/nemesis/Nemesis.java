package green_cloud.master_hosts.nemesis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.simgrid.msg.Host;
import org.simgrid.msg.HostNotFoundException;
import org.simgrid.msg.Msg;
import org.simgrid.msg.MsgException;

import green_cloud.master_hosts.MasterVMManager;

import green_cloud.vm.VmDescription;
import green_cloud.vm.VmInterface;
import green_cloud.workers_hosts.WorkerVMManager;

public class Nemesis extends MasterVMManager {
	
	
	// PARAMETERS
	
	/**
	 * The maximum number of time-slot evaluated
	 */
	private int n_eval = 20;
	
	/**
	 * The power cost of a host idle.
	 */
	private double p_idle;
	
	/**
	 * The power cost of a host off.
	 */
	private double p_down;
	
	/**
	 * The power cost of using a vCPU.
	 */
	private double p_core;
	
	/**
	 * The power cost of switching ON a host.
	 */
	private double p_on;
	
	/**
	 * The power cost of switching OFF a host.
	 */
	private double p_off;
	
	/**
	 * The ideal green power production trajectory (format {(time,power)*).
	 * Used to scale the actual power production.
	 */
	private TreeMap<Double,Double> idealGreenPow;
	
	/**
	 * The size of the green power production historic we used to compute the standard deviation. 
	 */
	private double stdDevPeriod;
	
	/**
	 * The bandwidth available on one route to migrate the VMs (considered to be the same for all the routes).
	 */
	private double bandwidth;

	/**
	 * The minimum latency to go from a DC to another one.
	 */
	private double minInterDClatency;
	
	/**
	 * The maximum latency to go from a DC to another one.
	 */
	private double maxInterDClatency;
	
	/**
	 * The latency to go from a server to another one in the same DC.
	 */
	private double intraDClatency;
	
	// CONSTANT
	
	/**
	 * The size of the TCP window.
	 */
	private final static double windowSize = 4194304;
	
	/**
	 * Arbitrary parameter of SimGrid for computing TCP latency.
	 */
	private final static double gamma = 20537;
	
	/**
	 * Arbitrary parameter of SimGrid for computing migration time.
	 */
	private final static double alpha = 13.01;

	/**
	 * Arbitrary parameter of SimGrid for computing logical bandwidth.
	 */
	private static final double bandwidthRatio = 0.97;
		
	
	// UTILITARY ATTRIBUTES
	
	/**
	 * For each DC, the gap between the last green power received and the reference green trajectory
	 */
	private HashMap<String,Double> gap;
	
	/**
	 * The historic (in the format {(time,power)}*) of the green power production of each DC.
	 * Used to compute the standard deviation of the green power productions.
	 */
	private HashMap<String, TreeMap<Double,Double>> greenPower;
	
	/**
	 * The historic (in the format {(time,power)}*) of the power consumption of each DC.
	 */
	private HashMap<String, TreeMap<Double,Double>> consumptions_historic;
	
	/**
	 * Number of periods (e.g. days) of green power recordings passed.
	 */
	private int nbPeriod;
	
	/**
	 * Number of PV in each DC (format {(dcName,nbPV)}*)
	 */
	private HashMap<String,Integer> nbPv;
	
	/**
	 * The power consumption of each DC considering the preallocations previously made.
	 */
	private HashMap<String, Double> powerConsumptions;
	
	/**
	 * The list of migrations to perform.
	 */

	
	/**
	 * The ID of the DC sorted in an arbitrary order.
	 */
	private String[] sortedDC;
	
	
	
	/**
	 * Cost of buying energy (in euros/kWh)
	 */
	private double buying_cost;
	
	/**
	 * Gain of selling energy (in euros/kWh).
	 */
	private double selling_gain;
	
	/**
	 * Cost of negative auto-consomation gap (in euros/MWh).
	 */
	private double autoconso_cost;
	
	/**
	 * Gain of positive auto-consomation gap (in euros/MWh).
	 */
	private double autoconso_gain;
	
	/**
	 * Price of power network utilisation at peak hours (in euros/kWh).
	 */
	private double fullUtilisationCost;
	
	/**
	 * Price of power network utilisation at off-peak hours (in euros/kWh).
	 */
	private double lightUtilisationCost;
	
	/**
	 * Duration of peak time (in sec).
	 */
	private double fullCostPeriod;
	
	/**
	 * Duration of off-peak time (in sec).
	 */
	private double lightCostPeriod;
	
	/**
	 * Period of auto-conso paying (in sec).
	 */
	private double autoconsoPeriod ;
	
	/**
	 * Period of energy trading (in sec). 
	 */
	private double sellBuyPeriod;
	
	/**
	 * If true, we try to minimize the global financial cost of the cloud (i.e. SCORPIUS algorithm),
	 * else we try to minimize brown energy consumption of the DC (i.e. NEMESIS).
	 */
	private boolean optimizeFinancialCost;
	
		

	
	
	/**
	 * Create and deploys NEMESIS.
	 * @param host The host where the master must be executed.
	 * @param dcName The name of the data-centers which must be considered by the master.
	 * @param aHostPoweringOnDelay Duration a host takes to power ON.
	 * @param aHostPoweringOffDelay Duration a host takes to power OFF.
	 * @param inputWorkloadPath CSV file containing the incoming workload (line format : VM-arrival-time;VM-name;number-of-cores;duration).
	 * @param aWorkloadScaleFactor Factor used to scale the incoming workload to the cloud.
	 * @param inputGreenProductions CSV files containing the green power production of one photo-voltaic panel of each data-centers (line format : time;power-production).
	 * @param numberOfPVPerDC numbers of photo-voltaic panels (used to multiply the green production).
	 * @param aTimeslot duration of a time-slot.
	 * @param powerLossInMigration The ratio of power lost when migrating power from one DC to another one.
	 * @param aVmRamSizePerCore RAM size of the VM per core required.
	 * @param aStdDevPeriod The size of the green power production historic we used to compute the standard deviation. 
	 * @param idealGreenPowPath The path of the CSV file containing the historic (in the format {(time,power)}*) of the green power production of any DC (used to compute the standard deviation of the green power productions).
	 * @param aPidle The power cost of a host idle.
	 * @param aPdown The power cost of a host off.
	 * @param aPon The power cost of switching ON a host.
	 * @param aPoff The power cost of switching OFF a host.
	 * @param aPcore  The power cost of using a vCPU.
	 * @param aBandwidth The bandwidth available on one route to migrate the VMs (considered to be the same for all the routes).
	 * @param aMinInterDCLatency The minimum latency to go from on DC to another one (indexed by source and then by destination).
	 * @param aMaxInterDCLatency The maximum latency to go from on DC to another one (indexed by source and then by destination).
	 * @param theSortedDc The list of DC's IDs sorted in the order they must be considered by the algorithm. 
	 * @throws HostNotFoundException
	 */
	public Nemesis(Host host, String[] dcName, double aHostPoweringOnDelay, double aHostPoweringOffDelay, String inputWorkloadPath, int aWorkloadScaleFactor, String[] inputGreenProductions, int[] numberOfPVPerDC, int aTimeslot, double powerLossInMigration, int aVmRamSizePerCore, double aStdDevPeriod, String idealGreenPowPath, double aPidle, double aPdown, double aPon, double aPoff, double aPcore, double aBandwidth, double aMinInterDCLatency, double aMaxInterDCLatency, double anIntraDCLatency, String[] theSortedDc,  double aBuying_cost, double aSelling_gain, double anAutoconso_cost, double anAutoconso_gain, double aFullUtilisationCost, double aLightUtilisationCost, double aFullCostPeriod, double aLightCostPeriod, double anAutoconsoPeriod, double aSellBuyPeriod, boolean isOptimizeFinancialCost) throws HostNotFoundException {
		super(host, dcName, aHostPoweringOnDelay, aHostPoweringOffDelay, inputWorkloadPath, aWorkloadScaleFactor, inputGreenProductions, numberOfPVPerDC, aTimeslot, powerLossInMigration, aVmRamSizePerCore, aBuying_cost, aSelling_gain, anAutoconso_cost, anAutoconso_gain, aFullUtilisationCost, aLightUtilisationCost, aFullCostPeriod, aLightCostPeriod, anAutoconsoPeriod, aSellBuyPeriod,true);
	
		sortedDC = theSortedDc;		
				
		gap = new HashMap<String,Double>();
		greenPower = new HashMap<String, TreeMap<Double,Double>>();
		consumptions_historic = new HashMap<String, TreeMap<Double,Double>>();
		nbPv = new HashMap<String,Integer>();
		powerConsumptions = new HashMap<String,Double>();
		
		int i=0;
		
		for(String dc : dcName){
			gap.put(dc,0.);
			greenPower.put(dc, new TreeMap<Double,Double>());
			consumptions_historic.put(dc, new TreeMap<Double,Double>());
			nbPv.put(dc, numberOfPVPerDC[i]);
			
			i++;
		}
		
		nbPeriod = 0;
		stdDevPeriod = aStdDevPeriod;
		
		
		p_core = aPcore;
		p_idle = aPidle;
		p_on = aPon;
		p_off = aPoff;
		p_down = aPdown;
		
		minInterDClatency = aMinInterDCLatency;
		maxInterDClatency = aMaxInterDCLatency;
		intraDClatency = anIntraDCLatency;
		bandwidth = aBandwidth;
		
		buying_cost = aBuying_cost;
		selling_gain = aSelling_gain; 
		autoconso_cost = anAutoconso_cost;
		autoconso_gain = anAutoconso_gain; 	
		fullUtilisationCost = aFullUtilisationCost; 
		lightUtilisationCost = aLightUtilisationCost; 
		fullCostPeriod = aFullCostPeriod;
		lightCostPeriod = aLightCostPeriod; 
		autoconsoPeriod = anAutoconsoPeriod; 
		sellBuyPeriod = aSellBuyPeriod;
		
		optimizeFinancialCost = isOptimizeFinancialCost;		
		
		
	}


	@Override
	protected void deployVMs(ArrayList<VmDescription> vms) throws MsgException {	
			
		
		// first, we initialize power consumptions.
		//updateState();
		
		//updateConsumptionsHistoric();
		
		// then, we deploy the VMs.
		nemesisBehavior(vms);
		
		// finally, we update the green power consumptions (it happens after the decision because we base our decision only on the previous time-step values).
		//updateGreenPow();
	}
	
	
	// MAIN ALGORITHMS

	/**
	 * Main NEMESIS algorithm. 
	 * @param vms The list of VMs to deploy for the current time-slot.
	 * @throws MsgException 
	 */
	private void nemesisBehavior(ArrayList<VmDescription> vms) throws MsgException{
		for(ArrayList<WorkerVMManager> list : workerList.values()) {
			
			for(WorkerVMManager worker : list) {
				powerOnHost(worker);

			}
			
		}
	}

}
