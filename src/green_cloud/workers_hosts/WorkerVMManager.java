package green_cloud.workers_hosts;

import java.util.ArrayList;
import java.util.Date;

import org.simgrid.msg.Host;
import org.simgrid.msg.HostNotFoundException;
import org.simgrid.msg.Msg;
import org.simgrid.msg.MsgException;
import org.simgrid.msg.Process;
import org.simgrid.msg.Task;
import org.simgrid.msg.TaskCancelledException;

import green_cloud.master_hosts.MasterVMManager;


/**
 * Worker application which encapsulate the desired behavior of a hosts.
 * It also contains some primitives which simplify the master works.
 * This applications is triggered by the master.
 * @author bcamus
 *
 */
public class WorkerVMManager extends Process {


	
	/**
	 * Amount of Mbyte/s allocated to the VM migration (cannot be larger than net_cap). Use 0 if unsure. (according to http://simgrid.gforge.inria.fr/simgrid/3.14/doc/).
	 */
	public final static int VM_MIG_NET_SPEED = 0;
	
	/**
	 * Dirty page percentage of the VM according to migNetSpeed, [0-100]. Use 0 if unsure. (according to http://simgrid.gforge.inria.fr/simgrid/3.14/doc/).
	 */
	public final static int VM_DP_INTENSITY = 0;

	/**
	 * Value of the worker status when OFF.
	 */
	public final static int OFF_STATUS = 0;
	
	/**
	 * Value of the worker status when powering ON.
	 */
	public final static int POWERING_ON_STATUS = 1;
	
	/**
	 * Value of the worker status when ON.
	 */
	public final static int ON_STATUS = 2;
	
	/**
	 * Value of the worker status when powering OFF.
	 */
	public final static int POWERING_OFF_STATUS = 3;

	/**
	 * Name of the task received for finalizing the worker.
	 */
	public static final String FINALIZE_TASK = "finalize";

	/**
	 * Name of the data-center where the worker host belongs.
	 */
	private String siteName;
	
	/**
	 * Status of the worker (i.e. ON, OFF, POWERING_ON, POWERING_OFF).
	 */
	private int status;
	
	/**
	 * Duration the host takes to power ON.
	 */
	private double poweringOnDelay;
	
	/**
	 * Duration the host takes to power OFF.
	 */
	private double poweringOffDelay;
	

	

	/**
	 * Master of the worker.
	 */
	// we do this because of a bug in SimGrid (i.e. a Process can not shut down it's own host) to shutdown from a distant host. remove when the bug is fixed.
	private MasterVMManager master;
	
	
	/**
	 * Create a worker in a given host.
	 * @param host The host where the worker must be deployed.
	 * @param name The name of the worker.
	 * @param aPoweringOnDelay The duration the host takes to power ON.
	 * @param aPoweringOffDelay The duration the host takes to power OFF.
	 * @param aSiteName The name of the data-center where the worker host belongs.
	 * @param aMaster The master of the worker.
	 * @throws HostNotFoundException
	 */
	public WorkerVMManager(Host host, String name, double aPoweringOnDelay, double aPoweringOffDelay, String aSiteName, MasterVMManager aMaster) throws HostNotFoundException  {
	    super(host, name);
	    

	    poweringOnDelay = aPoweringOnDelay;
	    siteName = aSiteName;
	    poweringOffDelay = aPoweringOffDelay;
	    master = aMaster;
		

	    
	}
	
	/**
	 * Return the name of the data-center where the worker host belongs.
	 * @return the name of the data-center where the worker host belongs.
	 */
	public String getSiteName() {
		return siteName;
	}

	/**
	 * Worker behavior. It simply wait to receive message 
	 */
	@Override
	public void main(String[] arg0) throws MsgException {

		poweringOn();

		while(true) {  
		      Task task = Task.receive(getName());

		      if (FINALIZE_TASK.equals(task.getName())) {
		        break;
		      }
		      Msg.info("Received \"" + task.getName() +  "\". Processing it.");
		      try {

		    	  
		    	  task.execute();
		      } catch (TaskCancelledException e) {
		        e.printStackTrace();

		      }
		 }	    

	 
	}
	
	/**
	 * Return the master of the worker
	 * @return The master of the worker
	 */
	public MasterVMManager getMaster() {
		return master;
	}


	
	/**
	 * Start the powering ON process for the worker host : the host first enters the POWERING_ON mode for a given duration before actually powering ON.
	 * @throws MsgException
	 */
	private void poweringOn() throws MsgException{
		
		Msg.info("I'm powering on... and I am "+this);		
		getHost().setPstate(2);			
		waitFor(poweringOnDelay);		
		getHost().setPstate(0);
		Msg.info("I'm on : "+this);
	}
	
	/**
	 * Return the status of the host (i.e. ON, OFF, POWERING_ON, POWERING_OFF).
	 * @return The status of the host
	 */
	public int getStatus() {
		return status;
	}
	
}
