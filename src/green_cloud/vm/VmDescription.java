package green_cloud.vm;

/**
 * Description of a VM.
 * Used by the master in order to load the incoming workload (i.e. when the VM is not yet created). 
 * @author bcamus
 *
 */
public class VmDescription implements VmInterface{
	
	/**
	 * Name of the VM.
	 */
	private String name;
	
	/**
	 * Number of cores used by the VM.
	 */
	private int nbCore;
	
	/**
	 * Duration of the VM execution.
	 */
	private double duration;
	
	
	/**
	 * The time instant that the VM request was received
	 */
	private double submissionTime;
	
	
	/**
	 * percentage of cores really used by the VM.
	 */
	private double use;
	
	/**
	 * Creates a VM description.
	 * @param name Name of the VM.
	 * @param nbCore Number of cores used by the VM.
	 * @param duration Duration of the VM execution.
	 */
	public VmDescription(String name, int nbCore, double duration, double aUse, double aSubmissionTime) {
		super();
		this.name = name;
		this.nbCore = nbCore;
		this.duration = duration;
		this.use = aUse;
		this.setSubmissionTime(aSubmissionTime);
	}
	
	/**
	 * Return the VM name.
	 * @return The VM name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set the VM name.
	 * @param name The new VM name.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Return the number of cores used by the VM.
	 * @return The number of cores used by the VM.
	 */
	public int getNbCores() {
		return nbCore;
	}
	
	/**
	 * Set the number of cores used by the VM.
	 * @param nbCore The number of cores used by the VM.
	 */
	public void setNbCore(int nbCore) {
		this.nbCore = nbCore;
	}
	
	public double getUse() {
		return use;
	}

	public void setUse(double use) {
		this.use = use;
	}

	/**
	 * Return the duration of execution of the VM.
	 * @return The duration of execution of the VM.
	 */
	public double getRemainingDuration() {
		return duration;
	}
	
	/**
	 * Set the duration of execution of the VM.
	 * @param duration The duration of execution of the VM.
	 */
	public void setRemainingDuration(double duration) {
		this.duration = duration;
	}
	
	/**
	 * Return the submission time of the VM.
	 * @return The submission time of the VM.
	 */
	public double getSubmissionTime() {
		return submissionTime;
	}
	
	/**
	 * Set the time the VM was submitted.
	 * @param submissionTime The instant of time that the VM was submitted.
	 */
	public void setSubmissionTime(double submissionTime) {
		this.submissionTime = submissionTime;
	}
	
	
	public String toString(){
		return "VM required : "+getName()+" | nbCores: "+nbCore+" \n duration : "+duration + " submission time: "+submissionTime;
	}
	
}
