package green_cloud.vm;

public interface VmInterface {
	
	/**
	 * Return the VM name.
	 * @return The VM name.
	 */
	public String getName();
		
	/**
	 * Return the number of cores used by the VM.
	 * @return The number of cores used by the VM.
	 */
	public int getNbCores();
	
	
	/**
	 * Return the remaining duration of execution of the VM.
	 * @return The remaining duration of execution of the VM.
	 */
	public double getRemainingDuration();
	
}
