package green_cloud;

import org.simgrid.msg.Host;
import org.simgrid.msg.HostNotFoundException;
import org.simgrid.msg.Msg;

import green_cloud.master_hosts.nemesis.Nemesis;

/**
 * Main class which launch the simulation of a green cloud.
 * It installs a master application. The master application:
 * 		- installs workers on all hosts,
 * 		- instantiate the electric system of the cloud which receives incoming photo-voltaic power productions of each data-centers given by input traces.
 * 		- receives an incoming workload composed of heterogeneous VM
 * 		- deploy the VM to the worker.
 * @author bcamus
 *
 */
public class Main {
	
	/**
	 * The file containing the incoming workload for the cloud
	 */
	private final static String inputWorkloadPath = "input/workload/very-very-small-workload-duration-timeslot.csv";
	//private final static String inputWorkloadPath = "input/workload/google_trace_from_600.0_to_605400.0.csv";
	
	//private final static String inputWorkloadPath = "input/workload/dummy-workload.csv";
	
	/**
	 * Factor used to scale the incoming workload to the cloud. It simply duplicate each incoming VM of the workload.
	 */
	private final static int totalNbCores = (1035 - 1) * 6;
	private final static int workloadMaxNbCoresUsed = 564;
	private final static int workloadScaleFactor = (int)((totalNbCores * 0.8) / workloadMaxNbCoresUsed);
	//private final static int workloadScaleFactor = 1;
	
	/**
	 * The SimGrid XML file of the cloud platform description.
	 */
	//private final static String platformFile = "/Users/bcamus/Documents/workspace/SimGrid/input/platform/grid5000Pstate.xml";
	private final static String platformFile = "input/platform/homogeneousGrid5000Pstate.xml";
	
	/**
	 * The name of the data-centers which must be considered by the master.
	 * NB: the order is the same for @nbPV et @pvTraces.
	 */
	private final static String[] dcName = {"lille","luxembourg","nancy","toulouse","sophia","grenoble","lyon","rennes","reims"};
	
	/**
	 * The number of photo-voltaic panels in each data-centers (used to scale the photo-voltaic input traces)
	 */
	private final static int[] nbPV = {74*30/80,38*30/80,240*30/80,115*30/80,129*30/80,140*30/80,103*30/80,151*30/80,44*30/80};
	
	/**
	 * The files containing the photo-voltaic power productions traces of each data-centers for a single photo-voltaic panel
	 */
	private final static String[] pvTraces = {"input/photo-voltaic/winter.csv","input/photo-voltaic/autumn2.csv","input/photo-voltaic/autumn1.csv","input/photo-voltaic/spring.csv","input/photo-voltaic/summer.csv","input/photo-voltaic/trace-6.csv","input/photo-voltaic/trace-7.csv","input/photo-voltaic/trace-8.csv","input/photo-voltaic/trace-9.csv"};
	
	/**
	 * The name of the data-centers sorted in the order they must be considered by the master.
	 */
	// BEST
	private final static String[] sortedDc = {"grenoble","rennes","sophia","lyon","toulouse","nancy","reims","luxembourg","lille"};
	// WORST
	//private final static String[] sortedDc = {"lille","luxembourg","reims","nancy","toulouse","lyon","sophia","rennes","grenoble"};
	
	/**
	 * The duration a host takes to power ON
	 */
	private final static double hostPoweringOnDelay = 150;
	
	/**
	 * The duration a host takes to power OFF
	 */
	private final static double hostPoweringOffDelay = 10;
	
	/**
	 * The duration of a master time-slot 
	 */
	private final static int timeslot = 300;
	
	/**
	 * RAM size of the VM per core required.
	 */
	private final static int VM_RAMSIZE_PER_CORE = 2048;
	
	/**
	 * The ratio of power lost when migrating power from one DC to another one.
	 */
	private final static double powerLossInMigration = 0.06;
	
	/**
	 * The trace level desired in SimGrid log (see SimGrid for more information).
	 */
	private final static String traceLevel = "info";
	
	/**
	 * The file where SimGrid log must be written
	 */
	private final static String outputLogFile = "output/simgridresult.log";
	
	// NEMESIS PARAMETERS
	
	/**
	 * The size of the green power production historic SAGITTA used to compute the standard deviation. 
	 */
	private final static double stdDevPeriod = 24 * 60 * 60;
	
	/**
	 * The path of the CSV file containing the historic (in the format {(time,power)}*) of the green power production of any DC (used to compute the standard deviation of the green power productions).
	 */
	private final static String idealGreenPowPath = "input/photo-voltaic/Perfect-Trace-Model.csv";
	
	/**
	 * The power cost of a host idle.
	 */
	private final static double pidle = 97;
			
	/**
	 * The power cost of a host off.
	 */
	private final static double pdown = 8;
	
	/**
	 * The power cost of switching ON a host.
	 */
	private final static double pon = 127;
	
	/**
	 * The power cost of switching OFF a host.
	 */
	private final static double poff = 200;
	
	/**
	 * The power cost of using a vCPU.
	 */
	private final static double pcore = 20.5;
	
	/**
	 * The bandwidth available on one route to migrate the VMs (considered to be the same for all the routes).
	 */
	// TODO: we divide by 1.05 because of a simgrid bug with cross traffic. Remove when fixed. 
	private final static double bandwidth = 1000000000./(8. * 1.05);
	
	
	
	/**
	 * The minimum latency to go from on DC to another one (indexed by source and then by destination).
	 */
	private final static double minInterDClatency = 9 * 0.00225;
	
	/**
	 * The maximum latency to go from on DC to another one (indexed by source and then by destination).
	 */
	private final static double maxInterDClatency = 5 * 0.00225;
	
	
	
	/**
	 * The latency to go from a server to another one in the same DC.
	 */
	private final static double intraDClatency = 2 * 0.00225;
	
	/*
	 * FINANCIAL MODEL PARAMETERS.
	 */
	
	/**
	 * Cost of buying energy (in euros/kWh)
	 */
	private final static double aBuying_cost = 0.15 ;
	
	/**
	 * Gain of selling energy (in euros/kWh).
	 */
	private final static double aSelling_gain = 0.06;
	
	/**
	 * Cost of negative auto-consomation gap (in euros/MWh).
	 */
	private final static double anAutoconso_cost = 63.63;
	
	/**
	 * Gain of positive auto-consomation gap (in euros/MWh).
	 */
	private final static double anAutoconso_gain = 54.21;
	
	/**
	 * Price of power network utilisation at peak hours (in euros/kWh).
	 */
	private final static double aFullUtilisationCost = 0.0418;
	
	/**
	 * Price of power network utilisation at off-peak hours (in euros/kWh).
	 */
	private final static double aLightUtilisationCost = 0.0281;
	
	/**
	 * Duration of peak time (in sec).
	 */
	private final static double aFullCostPeriod = 16 * 3600;
	
	/**
	 * Duration of off-peak time (in sec).
	 */
	private final static double aLightCostPeriod = 8 * 3600;
	
	/**
	 * Period of auto-conso paying (in sec).
	 */
	private final static double autoconsoPeriod = 30 * 60;
	
	/**
	 * Period of energy trading (in sec). 
	 */
	private final static double sellBuyPeriod = 10 * 60;
	
	/**
	 * if true, we optimize the financial costs (i.e. SCORPIUS algorithm) rather than brown energy (i.e. NEMESIS).
	 */
	private final static boolean optimizeFinancialCosts = true;
	
	/**
	 * if true, the hosts are initially OFF and the controller switch OFF hosts when they are empty.
	 */
	private final static boolean shutDownEmptyHost = true;
	
	
	private final static double useThreshold = 0.5;
	
	/**
	 * Launch the simulation.
	 * @param args Not used.
	 * @throws HostNotFoundException
	 */
	public static void main(String[] args) throws HostNotFoundException {
		
		Msg.energyInit();
		
		Msg.init(new String[]{"--log=root.threshold:"+traceLevel,"--log=root.app:file:"+outputLogFile});
		
		Msg.createEnvironment(platformFile);
		
		new Nemesis(Host.all()[0],dcName, hostPoweringOnDelay, hostPoweringOffDelay,inputWorkloadPath,workloadScaleFactor,pvTraces,nbPV,timeslot, powerLossInMigration, VM_RAMSIZE_PER_CORE, stdDevPeriod, idealGreenPowPath, pidle, pdown, pon, poff, pcore, bandwidth, minInterDClatency, maxInterDClatency, intraDClatency, sortedDc,aBuying_cost, aSelling_gain, anAutoconso_cost, anAutoconso_gain, aFullUtilisationCost, aLightUtilisationCost, aFullCostPeriod, aLightCostPeriod, autoconsoPeriod, sellBuyPeriod, optimizeFinancialCosts).start();
		
		Msg.run();
		
	}
}
