package green_cloud.energy;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

import org.simgrid.msg.Host;
import org.simgrid.msg.Msg;
import org.simgrid.msg.MsgException;
import org.simgrid.msg.Process;

import green_cloud.master_hosts.MasterVMManager;

/**
 * A process monitoring the power production of a data-center from an input trace.
 * As soon as the power production change, the process notify an update to the electric grid through the master.
 * @author bcamus
 *
 */
public class PVProcess extends Process {

	/**
	 * The input file containing the power production trajectory.
	 */
	private BufferedReader input;
	
	/**
	 * The next green power production according to the input trace.
	 */
	private double nextGreenPowerProduction;
	
	/**
	 * The current green power production according to the input trace.
	 */
	private double currentGreenPowerProduction;
	
	/**
	 * the last time the power production was updated.
	 */
	private double time;
	
	/**
	 * the next time the power production will be updated.
	 */
	private double nextTime;
	
	
	/**
	 * Creates a process monitoring the power production of a data-center from an input trace.
	 * @param host the where the process should run.
	 * @param name the name of the process.
	 * @param inputFile the file containing the photo-voltaic power production trajectory.
	 */
	public PVProcess(Host host, String name, String inputFile) {
		super(host, name);
	//	try {
		//	input = new BufferedReader(new FileReader(inputFile));
			//nextGreenPowerProduction = Double.parseDouble(input.readLine().split(MasterVMManager.FILE_SEPARATOR)[1]);
			//advanceTime();
			time = 0;
			nextTime=300;
			
	/*	} catch (NumberFormatException | IOException | Error e) {
			e.printStackTrace();
			System.out.println(e);
			System.out.println(e.getMessage());
			System.out.println(new Date());
		}*/

	}
	
	/**
	 * advance to the new update of the power production -i.e. change the current/next time/production according to the trace.
	 */
	/*private void advanceTime() {
		try {
			String nextLine = input.readLine();
			if(nextLine != null){
				currentGreenPowerProduction = nextGreenPowerProduction;
				String[] nextInput = nextLine.split(MasterVMManager.FILE_SEPARATOR);
				nextGreenPowerProduction = Double.parseDouble(nextInput[1]);
				time = Msg.getClock();
				nextTime = Double.parseDouble(nextInput[0]);
			}else{
				nextTime = Double.MAX_VALUE;
			}
		
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e);
			System.out.println(e.getMessage());
			System.out.println(new Date());
		}
	}
*/
	/**
	 * Main behavior : update the current power production according to the times and values in the input trace.
	 */
	@Override
	public void main(String[] arg0) throws MsgException {
		Msg.info("I start monitoring the green power production of DC "+getName());
		while(true){
			waitFor(nextTime - time);
			//MasterVMManager.updateElectricDC();
		//	advanceTime();
			// we do not forget to update the power exchange which may have change due to the new power production
			//MasterVMManager.updatePowerMigrations();
			Msg.info("new green power production for DC "+getName()+" : "+currentGreenPowerProduction+"W");
		}
		
	}
	
	/**
	 * Return the current green power production according to the input trace and the power migrating from/to this data-center.
	 * @return The current green power production.
	 */
	public double getCurrentGreenPowerProduction() {
		return currentGreenPowerProduction;
	}

}
