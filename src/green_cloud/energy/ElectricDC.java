package green_cloud.energy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import org.simgrid.msg.Host;

import green_cloud.master_hosts.MasterVMManager;
import green_cloud.workers_hosts.WorkerVMManager;

/**
 * The electric system of a single data-center.
 * @author bcamus
 *
 */
public class ElectricDC extends ElectricSystem{
			
	/**
	 * The name of the data-center.	
	 */
	private String id;
	
	/**
	 * The process monitoring the photo-voltaic power production of a single photo-voltaic panel in the data-center.
	 * (NB: we assume that all the photo-voltaic panels of a data-centers have the same production)
	 */
	private PVProcess pv;
	
	
	/**
	 * The number of photo-voltaic panel in the data-center.
	 * Used to scale the photo-voltaic power production.
	 */
	private int nbPV;
	
	/**
	 * The power output stream of the system.
	 */
	private BufferedWriter power_output;
	
	/**
	 * The power production which SHOULD migrate from this data-center. 
	 * It differs to outputMigration which corresponds to the actual power migration performed.
	 */
	private HashMap<ElectricDC,Double> requiredOutputMigration;
	
	/**
	 * The power production which actually migrate from the data-center.  
	 */
	private HashMap<ElectricDC,Double> actualOutputMigration;

	
	/**
	 * the power production migrating to this data-center.
	 */
	private ArrayList<ElectricDC> powerMigrationIn;
	
	/**
	 * The number of incoming power migrations which is not treated yet by the topological sort of the Electric Grid. 
	 * It is used by the ElectricGrid to determine the power output migrations of the DC.
	 */
	private int nbIncomingMigrations;
	
	/**
	 * Ratio of power lost when migrating power.
	 */
	private double powerLossInMigration;
	
	
	/**
	 * Creates the electric system of a single data-center.
	 * @param anId The name of the data-center.
	 * @param host The host where the process monitoring the photo-voltaic power production of the data-center must run.
	 * @param inputFile the input file containing the power production trajectory of a single photo-voltaic panel in the data-center.
	 * @param aNbPV The number of photo-voltaic panels in the data-center. Used to scale the photo-voltaic power production.
	 * @param aMaster The master of the cloud.
	 */
	public ElectricDC(String anId, Host host, String inputFile, int aNbPV, double aPowerLossInMigration) { 
		super("output/energy_"+anId+".csv");			
		id = anId;						
		pv = new PVProcess(host,"pv_"+anId,inputFile);					
		nbPV = aNbPV;		
		requiredOutputMigration = new HashMap<ElectricDC, Double>();
		powerMigrationIn = new ArrayList<ElectricDC>();
		actualOutputMigration = new HashMap<ElectricDC, Double>();
		nbIncomingMigrations = 0;		
		powerLossInMigration = aPowerLossInMigration;
	
		
	}

	
	@Override
	public void startPvProcess() {
		pv.start();		
	}
	
	
}
