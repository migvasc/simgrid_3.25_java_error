package green_cloud.energy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import green_cloud.master_hosts.MasterVMManager;

/**
 * An electric system of a distributed cloud.
 * According to the cloud consumption given by SimGrid, and to the green power production given by the traces (monitored by some dedicated process), it computes the cumulative :
 * 		- energy consumption,
 * 		- brown energy consumption,
 * 		- green energy production.
 * The system produce an output files which log these energy trajectories.
 * The system must be manually updated (through the master) : 
 * 		- by the workers when they change their power consumption,
 * 		- and by the photo-voltaic monitoring processes when the power production has changed.
 * @author bcamus
 *
 */
public abstract class ElectricSystem {
	
	/**
	 * The cumulative energy consumption of the system.
	 */
	protected double cumulativeEnergyConsumption;
	
	/**
	 * The cumulative brown energy consumption of the system.
	 */
	protected double cumulativeBrownEnergyConsumption;
	
	/**
	 * The cumulative green energy production of the system.
	 */
	protected double cumulativeGreenEnergyProduction;
	
	protected double cumulativeRemainingGreenEnergy;
	
	public double getCumulativeRemainingGreenEnergy() {
		return cumulativeRemainingGreenEnergy;
	}

	/**
	 * The output stream of the system.
	 */
	private BufferedWriter output;
	
	/**
	 * The time of the last update.
	 */
	protected double time;

	/**
	 * Initialize the system.
	 * @param outputFile The output file of the system.
	 */
	public ElectricSystem(String outputFile){
	
	}
	
	

	
	/**
	 * Start the process which monitor the photo-voltaic production.
	 */
	public abstract void startPvProcess();
	

	
}
