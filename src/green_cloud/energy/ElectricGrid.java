package green_cloud.energy;

import java.util.ArrayList;

import org.simgrid.msg.Host;
import org.simgrid.msg.Msg;

/**
 * The electric system of the cloud.
 * This system is composed of several subsystems : the electric systems of the data-centers composing the cloud.
 * @author bcamus
 *
 */
public class ElectricGrid extends ElectricSystem{
	
	/**
	 * The list of the electric systems of the data-centers composing the cloud.
	 */
	private ArrayList<ElectricDC> dc;
	

	/**
	 * Creates the electric system of the cloud, including its subsystems (i.e. the electric systems of the data-centers composing the cloud).
	 * @param id The names of the data-centers
	 * @param host The host where the processes monitoring the photo-voltaic power productions of the data-centers must run.
	 * @param inputFiles The input files containing the power production trajectory of a single photo-voltaic panel of each data-center.
	 * @param nbPV The number of photo-voltaic panels in each data-center. Used to scale the photo-voltaic power production of each data-center.
	 * @param aPowerLossInMigration The ratio of power lost when migrating power.
	 */
	public ElectricGrid(String id[], Host host, String inputFiles[], int nbPV[], double aPowerLossInMigration, double aBuying_cost, double aSelling_gain, double anAutoconso_cost, double anAutoconso_gain, double aFullUtilisationCost, double aLightUtilisationCost, double aFullCostPeriod, double aLightCostPeriod, double autoconsoPeriod, double sellBuyPeriod){
		super("output/global_energy.csv");
		dc = new ArrayList<ElectricDC>();
		
		for(int i=0;i<id.length;i++){
			dc.add(new ElectricDC(id[i],host,inputFiles[i],nbPV[i],aPowerLossInMigration));
		}	
		
	}
	

	@Override
	public void startPvProcess() {
		for(ElectricDC aDc : dc){
			aDc.startPvProcess();
		}
	}
	
	

}
