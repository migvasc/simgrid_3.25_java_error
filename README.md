# simgrid_3.25_java_error

Example of the code that results in the error using SimGrid MSG interface in Java. 

The main class of the project is green_cloud.Main.java. I've used SimGrid's jar library version 3.25, located at lib/simgrid.jar. Maybe it will be necessary to use another .jar file from version 3.25 since I compiled the one in the lib folder into my machine that runs Ubuntu 18.04

It is necessary to import the jar file to run the project into Eclipse: Project Properties -> Java Build Path -> Libraries -> Add External JARs ->  then the SimGrid jar file with version 3.25 should be selected.

The following error should occur:

```
#
# A fatal error has been detected by the Java Runtime Environment: 
#
#  SIGSEGV (0xb) at pc=0x00007f1dd75435d0, pid=24851, tid=0x00007f1e1936c700
#
# JRE version: Java(TM) SE Runtime Environment (8.0_191-b12) (build 1.8.0_191-b12)
# Java VM: Java HotSpot(TM) 64-Bit Server VM (25.191-b12 mixed mode linux-amd64 compressed oops)
# Problematic frame:
# C  [libsimgrid.so+0x2c05d0]  void simgrid::xbt::PropertyHolder::set_properties<std::unordered_map<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::hash<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::equal_to<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > > >(std::unordered_map<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::hash<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::equal_to<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > > const&)+0x90
#
# Failed to write core dump. Core dumps have been disabled. To enable core dumping, try "ulimit -c unlimited" before starting Java again
#
# An error report file with more information is saved as:
# /home/migvasc/PHD/nemesis/SimGrid/hs_err_pid24851.log
#
# If you would like to submit a bug report, please visit:
#   http://bugreport.java.com/bugreport/crash.jsp
# The crash happened outside the Java Virtual Machine in native code.
# See problematic frame for where to report the bug.
#
```

